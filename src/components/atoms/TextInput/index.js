import React from 'react';
import {StyleSheet, Text, View, TextInput as TextInputRN} from 'react-native';
import tw from 'twrnc'

const TextInput = ({placeholder, icon, label, ...restProps}) => {
  return (
    <>
      {
        label && (
          <Text style={tw`text-sm text-[#223263]`}>{label}</Text>
        )
      }
      <View style={tw`border border-[#9098B1] focus:border-[#40BFFF] rounded-md p-[0.05rem] flex-row items-center w-full`}>
        {icon && icon}
        <TextInputRN
          placeholder={placeholder}
          {...restProps}
        />
      </View>
    </>
  );
};

export default TextInput;

const styles = StyleSheet.create({
  input: {borderWidth: 1, borderColor: '#020202', borderRadius: 8, padding: 10},
});
