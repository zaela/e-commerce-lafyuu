import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {
  IcHomeOff,
  IcHomeOn,
  IcExploreOn,
  IcExploreOff,
  IcCartOn,
  IcCartOff,
  IcOfferOn,
  IcOfferOff,
  IcAccountOn,
  IcAccountOff,
} from '../../../assets';
import tw from 'twrnc';

const Icon = ({label, focus}) => {
  switch (label) {
    case 'Home':
      return focus ? <IcHomeOn /> : <IcHomeOff />;
    case 'Explore':
      return focus ? <IcExploreOn /> : <IcExploreOff />;
    case 'Cart':
      return focus ? <IcCartOn /> : <IcCartOff />;
    case 'Offer':
      return focus ? <IcOfferOn /> : <IcOfferOff />;
    case 'Account':
      return focus ? <IcAccountOn /> : <IcAccountOff />;
    default:
      return <IcHomeOn />;
  }
};

const BottomNavigator = ({state, descriptors, navigation}) => {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View style={tw`flex flex-row justify-between px-3 pt-2 pb-2`}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={tw`flex items-center`}  
          >
            <Icon label={label} focus={isFocused} />
            <Text style={tw`${isFocused ? 'text-[#40BFFF]' : 'text-[#9098B1]'} text-xs mt-1.5 `}>{label}</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default BottomNavigator;
