import { View, Text, ScrollView } from 'react-native'
import React from 'react'
import tw from 'twrnc'
import { Gap } from '../../atoms'

const HorizontalView = ({children, label, directionLabel, url}) => {
  return (
    <View>
      <View style={tw`flex-row justify-between`}>
        <Text style={tw`text-[#223263] text-base font-bold`}>{label}</Text>
        <Text style={tw`text-[#40BFFF] text-base font-bold`}>{directionLabel}</Text>
      </View>
      <Gap height="h-3"/>
      <ScrollView style={tw`flex-row gap-3`} horizontal={true} showsHorizontalScrollIndicator={false}>
      {children}
      </ScrollView>
    </View>
  )
}

export default HorizontalView