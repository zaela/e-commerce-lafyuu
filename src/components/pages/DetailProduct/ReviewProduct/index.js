import { View, Text, ScrollView, Image } from 'react-native'
import React from 'react'
import { Gap, Rating } from '../../../atoms'
import Reviewer from '../Reviewer'
import tw from 'twrnc'

const ReviewProduct = () => {
  return (
    <View>
        <View style={tw`flex-row justify-between`}>
            <Text style={tw`text-[#223263] text-base font-bold`}>Review Product</Text>
            <Text style={tw`text-[#40BFFF] text-base font-bold`}>See More</Text>
        </View>
        <Gap height="h-3"/>
        <Rating rate={4} rateNumber={4} rater={10}/>
        <Gap height="h-3"/>
        <View>
            <View>
                <Reviewer/>
                <Text style={tw`text-left text-gray-400 text-sm mt-2`}>
                air max are always very comfortable fit, clean and just perfect in every way. just the box was too small and scrunched the 
                sneakers up a little bit, not sure if the box was always this small but the 90s are and will always be one of 
                my favorites.
                </Text>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <View style={tw`flex-row gap-2`}>
                        <Image style={tw`max-w-22 max-h-22`} source={require('C:/nextjs_express/lafyuu/src/assets/images/sepatu1.png')}/>
                        <Image style={tw`max-w-22 max-h-22`} source={require('C:/nextjs_express/lafyuu/src/assets/images/sepatu2.png')}/>
                        <Image style={tw`max-w-22 max-h-22`} source={require('C:/nextjs_express/lafyuu/src/assets/images/sepatu3.png')}/>
                    </View>
                </ScrollView>
                <Text style={tw`text-left text-gray-400 text-sm mt-2`}>Januari 03, 2023</Text>
            </View>
        </View>
    </View>
  )
}

export default ReviewProduct