import { View, Text, FlatList } from 'react-native'
import React from 'react'
import { ProductCard } from '../../../molecules'
import tw from 'twrnc';

const RecomendedProduct = ({navigation}) => {
  const products = [
    {
      image:'C:/nextjs_express/lafyuu/src/assets/images/sepatu1.png',
      name:'FS - Nike Air Max 270 React',
      price:400000,
      discount:22,
    },
    {
      image:'C:/nextjs_express/lafyuu/src/assets/images/sepatu2.png',
      name:'AS - Adidas Air 270 React',
      price:330000,
      discount:30,
    },
    {
      image:'C:/nextjs_express/lafyuu/src/assets/images/tas1.png',
      name:'AS - Adidas Air 270 React',
      price:220000,
      discount:15,
    },
    {
      image:'C:/nextjs_express/lafyuu/src/assets/images/tas1.png',
      name:'Tas Air 270 Luxury React',
      price:530000,
      discount:34,
    },
  ]
  return (
    <View>
      <FlatList
        data={products}
        renderItem={({item}) => (
          <ProductCard width="w-43" product={item} navigation={navigation}/>
        )}
        //Setting the number of column
        numColumns={2}
        columnWrapperStyle={tw`flex flex-row justify-between my-2`}
        showsVerticalScrollIndicator={false}
        scrollEnabled={false}
        keyExtractor={(item, index) => index}
      />
    </View>
  )
}

export default RecomendedProduct