import { View, Text, Image, TouchableOpacity, TextInput } from 'react-native'
import React from 'react'
import tw from 'twrnc'
import { IcList, IcMinus, IcPlus, IcTrash, Love } from '../../../../assets'
import { Gap } from '../../../atoms'

const RowOrder = ({order, navigation}) => {
  return (
    <View style={tw`flex-row gap-3 p-3 border border-gray-300 rounded-md`}>
      <TouchableOpacity style={tw`w-1/5`}>
        <Image style={tw`h-14 w-14`} source={require('C:/nextjs_express/lafyuu/src/assets/images/sepatu1.png')}/>
      </TouchableOpacity>
      <View style={tw`flex-1`}>
        <View style={tw`flex-row justify-between`}>
          <Text style={tw`text-[#223263] text-sm font-bold w-3/4`}>
              {order.name}
          </Text>
          <View style={tw`flex-row gap-2 w-50`}>
            <TouchableOpacity>
              <Love width={24} height={24}/>
            </TouchableOpacity>
            
            <TouchableOpacity>
              <IcTrash width={24} height={24}/>
            </TouchableOpacity>
          </View>
        </View>
        <Gap height="h-2"/>
        <View style={tw`flex flex-row`}>
          <View style={tw`w-2/3 flex justify-center`}>
            <Text style={tw`text-[#40BFFF] text-sm font-bold`}>
                {order.price}
            </Text>
          </View>
          <View style={tw`flex-row gap-1 items-center w-60`}>
            <TouchableOpacity>
              <IcMinus width={22} height={22}/>
            </TouchableOpacity>
            <TextInput name="orderQty" value='0'/>
            <TouchableOpacity>
              <IcPlus width={22} height={22}/>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  )
}

export default RowOrder