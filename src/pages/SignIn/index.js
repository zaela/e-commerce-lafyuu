import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import { Facebook, Google, LogoBlue, Message, Password } from '../../assets'
import tw from 'twrnc'
import { Button, Gap, TextInput } from '../../components'

const SignIn = ({navigation}) => {
  return (
    <View>
      <View style={tw`flex items-center justify-center bg-white mt-30`}>
        <LogoBlue/>
        <Gap height='h-4'/>
        <Text style={tw`text-[#223263] font-bold text-base font-serif tracking-wide`}>Welcome to Lafyuu</Text>
        <Gap height='h-1'/>
        <Text style={tw`text-[#9098B1] font-bold text-xs font-serif tracking-wide`}>Sign in to continue</Text>
        <Gap height='h-4'/>
        <View style={tw`w-full px-4`}>
          <TextInput
            label="Email Address"
            placeholder="Type your email address"
            icon={<Message style={tw`px-4`}/>}
            value=''
            // onChangeText={(value) => setForm('email', value)}
          />
          <Gap height='h-3' />
          <TextInput
            label="Password"
            placeholder="Type your password"
            icon={<Password style={tw`px-4`}/>}
            value=''
            // onChangeText={(value) => setForm('password', value)}
            secureTextEntry
          />
          <Gap height='h-4' />
          <Button text="Sign In" padding='p-4' color='#40BFFF' textColor='#ffff' onPress={() => navigation.navigate('Home')} />
          <Gap height='h-4' />

          <Text style={tw`text-[#9098B1] text-sm text-center`}>OR</Text>
          <Gap height='h-4' />
          <View style={tw`flex-row items-center`}>
            <View style={tw`w-12 flex-row justify-center`}>
              <Google/>
            </View>
            <View style={tw`w-full`}>
              <Text style={tw`text-[#9098B1] text-sm text-center -ml-20`}>Login With Google</Text>
            </View>
          </View>
          <Gap height='h-4' />

          <View style={tw`flex-row items-center`}>
            <View style={tw`w-12 flex-row justify-center`}>
              <Facebook/>
            </View>
            <View style={tw`w-full`}>
              <Text style={tw`text-[#9098B1] text-sm text-center -ml-20`}>Login With Facebook</Text>
            </View>
          </View>
          <Gap height='h-6' />

          <Text style={tw`text-[#40BFFF] text-sm font-semibold text-center`}>Forgot Password</Text>
          <Gap height='h-2' />
          <View style={tw`flex-row justify-center`}>
            <View style={tw`flex-row`}>
              <Text style={tw`text-[#9098B1] text-sm text-center`}>Don't have an account ?</Text>
              <TouchableOpacity activeOpacity={0.7} onPress={() => navigation.navigate('SignUp')}>
                <Text style={tw`text-[#40BFFF] text-sm font-semibold text-center`}> Register</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  )
}

export default SignIn