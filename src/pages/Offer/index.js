import React from 'react';
import { Image, ImageBackground, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import tw from 'twrnc';
import { BannerSlider, Category, Gap, Header } from '../../components';
import { FlashSale, MegaSale, RecomendedProduct } from '../../components/pages';

const Offer = () => {
    return (
        <>
            <Header title="Offer" borderBottom />
            {/* <View style={tw`flex-1 bg-white px-4 w-full my-2`}> */}
            <ScrollView horizontal={false} showsHorizontalScrollIndicator={false} style={tw`flex-1 bg-white px-4 w-full`} >
                <Gap height="h-4"/>
                <View style={tw`w-full p-4 rounded-md bg-[#40BFFF]`}>
                    <Text style={tw`text-white text-base font-semibold text-left`}>Use “MEGSL” Cupon For Get 90%off</Text>
                </View>
                <Gap height="h-4"/>
                <TouchableOpacity style={tw`w-full mt-3`}>
                    <Image source={require('C:/nextjs_express/lafyuu/src/assets/images/offerBanner1.png')} style={tw`w-full`}/>
                </TouchableOpacity>
                <TouchableOpacity style={tw`w-full mt-3`}>
                    <Image source={require('C:/nextjs_express/lafyuu/src/assets/images/offerBanner2.png')} style={tw`w-full`}/>
                </TouchableOpacity>
            </ScrollView>
            {/* </View> */}
        </>
    )
}

export default Offer