import { View, Text } from 'react-native'
import React from 'react'
import tw from 'twrnc';

const Specification = () => {
  return (
    <View>
      <Text style={tw`text-[#223263] font-semibold text-base`}>Specification</Text>
      <View style={tw`flex gap-3 mt-2`}>
        <View style={tw`flex-row justify-between`}>
            <View>
                <Text style={tw`text-[#223263] text-sm`}>Shown:</Text>
            </View>
            <View>
                <Text style={tw`text-[#223263] text-gray-300 text-right`}>Laser Blue</Text>
                <Text style={tw`text-[#223263] text-gray-300 text-right`}>Laser Blue/Anthracite/Watermelon</Text>
                <Text style={tw`text-[#223263] text-gray-300 text-right`}>Watermelon/White</Text>
            </View>
        </View>
        <View style={tw`flex-row justify-between`}>
            <View>
                <Text style={tw`text-[#223263] text-sm`}>Style:</Text>
            </View>
            <View>
                <Text style={tw`text-[#223263] text-gray-300 text-right`}>CD0113-400</Text>
            </View>
        </View>
      </View>
    </View>
  )
}

export default Specification