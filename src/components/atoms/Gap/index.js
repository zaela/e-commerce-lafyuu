import React from 'react';
import {View} from 'react-native';
import tw from 'twrnc';

const Gap = ({width, height}) => {
  return <View style={tw`${width} ${height}`} />;
};

export default Gap;
