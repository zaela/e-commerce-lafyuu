import React from 'react';
import { Text, View } from 'react-native';
import tw from 'twrnc';
import { IcDate, IcGender, IcMessage, IcPhone, Password } from '../../assets';
import { Header, RowComponent } from '../../components';

const Profile = ({navigation}) => {
    return (
        <>
            <Header title="Profile" borderBottom onBack={() => navigation.navigate('Account')}/>
            <View style={tw`flex-1 bg-white w-full py-4`}>
                <RowComponent image key={11} title="Maximus Gold" subtitle="Manager"/>
                <RowComponent key={1} icon={<IcGender height={28} width={28}/>} 
                    title="Gender" navigation={() => navigation.navigate("")}
                    arrowRight={true} desc={<Text style={tw`text-sm text-gray-300 text-right`}>Male</Text>}
                />
                <RowComponent key={2} icon={<IcDate height={28} width={28}/>} 
                    title="Birthday" navigation={() => navigation.navigate("")}
                    arrowRight={true} desc={<Text style={tw`text-sm text-gray-300 text-right`}>12-12-2023</Text>}
                />
                <RowComponent key={3} icon={<IcMessage height={28} width={28}/>} 
                    title="Email" navigation={() => navigation.navigate("")}
                    arrowRight={true} desc={<Text style={tw`text-sm text-gray-300 text-right`}>Galaxy@gmail.com</Text>}
                />
                <RowComponent key={4} icon={<IcPhone height={28} width={28}/>} 
                    title="Phone Number" navigation={() => navigation.navigate("")}
                    arrowRight={true} desc={<Text style={tw`text-sm text-gray-300 text-right`}>(76464) - 87545647</Text>}
                />
                <RowComponent key={5} icon={<Password height={28} width={28}/>} 
                    title="Change Password" navigation={() => navigation.navigate("")}
                    arrowRight={true} desc={<Text style={tw`text-sm text-gray-300 text-right`}>...........</Text>}
                />
            </View>
        </>
    )
}

export default Profile