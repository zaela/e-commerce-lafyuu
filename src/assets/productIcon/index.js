import Dress from './dress.png';
import ManBag from './manBag.png';
import WomanBag from './womanBag.png';
import ManShoes from './manShoes.png';
import Shirt from './shirt.png';

export {
    Dress,
    ManBag,
    WomanBag,
    ManShoes,
    Shirt,
};