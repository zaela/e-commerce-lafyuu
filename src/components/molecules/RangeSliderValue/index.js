import { View, Text, ScrollView } from 'react-native'
import React, { useCallback, useState } from 'react'
import Slider from 'rn-range-slider';
import Thumb from './Slider/Thumb';
import Rail from './Slider/Rail';
import RailSelected from './Slider/RailSelected';
import Label from './Slider/Label';
import Notch from './Slider/Notch';
import TextButton from './TextButton';
import styles from './styles';

const RangeSliderValue = ({low, setLow, high, setHigh}) => {
  const [min, setMin] = useState(0);
  const [max, setMax] = useState(100000000);

  // const renderThumb = useCallback(
  //   (name: 'high' | 'low') => <Thumb name={name} />,
  //   [],
  // );
  const renderThumb = useCallback(() => <Thumb/>, []);
  const renderRail = useCallback(() => <Rail/>, []);
  const renderRailSelected = useCallback(() => <RailSelected/>, []);
  const renderLabel = useCallback(value => <Label text={value}/>, []);
  const renderNotch = useCallback(() => <Notch/>, []);
  const handleValueChange = useCallback((low, high) => {
    setLow(low);
    setHigh(high);
  }, []);

  return (
    <ScrollView>
      <View>
        <Slider
          style={styles.slider}
          min={min}
          max={max}
          step={1}
          renderThumb={renderThumb}
          renderRail={renderRail}
          renderRailSelected={renderRailSelected}
          renderLabel={renderLabel}
          renderNotch={renderNotch}
          onValueChanged={handleValueChange}
        />
      </View>
    </ScrollView>
  )
}

export default RangeSliderValue