import { View, Text } from 'react-native'
import React from 'react'
import tw from 'twrnc'

const LabelValue = ({label, value, labelStyle, valueStyle}) => {
  return (
    <View style={tw`flex-row justify-between`}>
      <Text style={tw`text-sm ${labelStyle ? labelStyle : 'text-gray-400'}`}>{label}</Text>
      <Text style={tw`text-sm ${valueStyle ? valueStyle : 'text-gray-500'}`}>{value}</Text>
    </View>
  )
}

export default LabelValue