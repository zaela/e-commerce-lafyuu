import { View, Text, Image, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import tw from 'twrnc'
import { IcArrowRight } from '../../../assets'

const RowComponent = ({title, icon, image, subtitle, navigation, desc, arrowRight}) => {
  const [bgColor, setBgColor] = useState('bg-white')

  return (
    <TouchableOpacity 
      style={tw`flex-row gap-3 p-4 px-4 ${bgColor}`}
      onPressIn={() => setBgColor('bg-[#EBF0FF]')}
      onPressOut={() => setBgColor('bg-white')}
      onPress={() => {
        navigation()
      }}
    >
        {
          image && (
            <View>
                  <Image style={tw`h-15 w-15 rounded-full`} source={require('C:/nextjs_express/lafyuu/src/assets/images/sepatu1.png')}/>
            </View>
            )
        }

        {
          icon && (
            <View>
              {icon}
            </View>
          )
        }

      <View style={tw`flex-row justify-between flex-1`}>
        <View>
          <Text style={tw`text-[#223263] text-base font-bold`}>
              {title}
          </Text>
          <Text style={tw`text-gray-300 text-sm font-semibold`}>
              {subtitle}
          </Text>
        </View>
      </View>
      
      {
        arrowRight && (
          <View style={tw`flex-row gap-3 justify-end flex-1`}>
            {
              desc && (
                desc
              )
            }
            <IcArrowRight/>
          </View>
        )
      }
    </TouchableOpacity>
  )
}

export default RowComponent