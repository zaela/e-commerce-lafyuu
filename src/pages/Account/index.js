import React from 'react';
import { ImageBackground, ScrollView, Text, View } from 'react-native';
import tw from 'twrnc';
import { IcAccountOn, IcBag, IcCreditCard, IcList, IcLocation, IcOfferOn, NotificationOn } from '../../assets';
import { BannerSlider, Category, Gap, Header } from '../../components';
import { FlashSale, MegaSale, RecomendedProduct } from '../../components/pages';
import { RowNotification } from '../../components/pages/Notification';

const Account = ({navigation}) => {
    return (
        <>
            <Header title="Account" borderBottom/>
            <View style={tw`flex-1 bg-white w-full py-4`}>
                <RowNotification key={1} icon={<IcAccountOn height={28} width={28}/>} title="Profile" navigation={() => navigation.navigate("Profile")}/>
                <RowNotification key={2} icon={<IcBag height={28} width={28}/>} title="Order" navigation={() => navigation.navigate("NotifFeed")}/>
                <RowNotification key={3} icon={<IcLocation height={28} width={28}/>} title="Address" navigation={() => navigation.navigate("NotifActivity")}/>
                <RowNotification key={4} icon={<IcCreditCard height={28} width={28}/>} title="Payment" navigation={() => navigation.navigate("NotifActivity")}/>
            </View>
        </>
    )
}

export default Account