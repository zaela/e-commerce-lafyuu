export default function currencyConverter(num) {
  if (!num) {
    return '-'
  }

  return num.toLocaleString('id-ID', {style: 'currency', currency: 'IDR', minimumFractionDigits: 0})
}
