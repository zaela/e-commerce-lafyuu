import React, { useEffect, useState } from 'react';
import {Dimensions, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Animated, { FadeIn, FadeOutUp, useAnimatedStyle, useSharedValue, withSpring, withTiming } from 'react-native-reanimated';
import {BlueSearch, IcBack, IcClose, IcFilter, IcSort, Love, Notification} from '../../../assets';
import tw from 'twrnc';
import { TextInput } from '../../atoms';
import RangeSliderValue from '../RangeSliderValue';
import currencyConverter from '../../../helpers/currencyConverter';
const {height, width} = Dimensions.get('window')

const Filter = ({filter, showFilter, open, setOpen}) => {
  const [low, setLow] = useState(0);
  const [high, setHigh] = useState(100000000);
  const progress = useSharedValue(open ? -width : width)
  // const progress = useSharedValue(0)

  const reanimatedStyle = useAnimatedStyle(() => {
    return {
      right:progress.value,
      height:height, 
      elevation:2,
    }
  }, [open])

  useEffect(() => {
    if (open) {
      progress.value = withTiming(0, {duration:1000})
    } else {
      progress.value = withTiming(-width, {duration:1000})
    }
  }, [open])
  return (
    <Animated.View
      entering={FadeIn}
      exiting={FadeOutUp}
      style={[tw`flex-1 bg-white z-3000 absolute`, reanimatedStyle]}
    >
      <ScrollView showsVerticalScrollIndicator={false} >
          <View style={tw`border-b border-gray-200`}>
            <View style={tw`flex-row gap-2 items-center px-4 py-4`}>
              <TouchableOpacity onPress={() => setOpen(false)}>
                <IcClose height={30} width={30} style={tw`text-gray-500`}/>
              </TouchableOpacity>
              <Text style={tw`text-[#223263] text-base font-bold`}>Filter Component</Text>
            </View>
          </View>
          <View style={tw`flex gap-2 px-4 pt-4`}>
            <Text style={tw`text-[#223263] text-base font-bold`}>Price Range</Text>
            <View style={tw`pt-3 pb-0 mb-0 flex-row justify-between w-full`}>
                <View style={tw`py-3 px-4 rounded-md border border-gray-300 w-2/5`}>
                  <Text style={tw`text-sm font-bold text-gray-400`}>{currencyConverter(low)}</Text>
                </View>
                <View style={tw`py-3 px-4 rounded-md border border-gray-300 w-2/5`}>
                  <Text style={tw`text-sm font-bold text-gray-400`}>{currencyConverter(high)}</Text>
                </View>
            </View>
            <View style={tw`-mt-6`}>
              <RangeSliderValue low={low} setLow={setLow} high={high} setHigh={setHigh} />
            </View>
            <View style={tw`py-2 flex-row justify-between`}>
              <Text style={tw`text-sm font-bold text-[#9098B1]`}>MIN</Text>
              <Text style={tw`text-sm font-bold text-[#9098B1]`}>MAX</Text>
            </View>
          </View>
          <View style={tw`flex gap-2 px-4 pt-4`}>
            <Text style={tw`text-[#223263] text-base font-bold`}>Condition</Text>
            <View style={tw`pt-3 pb-0 mb-0 flex flex-row flex-wrap gap-2 w-full`}>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md border border-gray-300`}>
                  <Text style={tw`text-sm font-bold text-gray-400`}>New</Text>
                </TouchableOpacity>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md bg-[#40BFFF] opacity-25`}>
                  <Text style={tw`text-sm font-bold text-opacity-100 text-sky-900`}>Used</Text>
                </TouchableOpacity>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md bg-[#40BFFF] opacity-25`}>
                  <Text style={tw`text-sm font-bold text-opacity-100 text-sky-900`}>Not Specified</Text>
                </TouchableOpacity>
            </View>
          </View>
          <View style={tw`flex gap-2 px-4 pt-4`}>
            <Text style={tw`text-[#223263] text-base font-bold`}>Buying Format</Text>
            <View style={tw`pt-3 pb-0 mb-0 flex flex-row flex-wrap gap-2 w-full`}>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md border border-gray-300`}>
                  <Text style={tw`text-sm font-bold text-gray-400`}>All Listings</Text>
                </TouchableOpacity>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md bg-[#40BFFF] opacity-25`}>
                  <Text style={tw`text-sm font-bold text-opacity-100 text-sky-900`}>Accepts Offers</Text>
                </TouchableOpacity>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md border border-gray-300`}>
                  <Text style={tw`text-sm font-bold text-gray-400`}>Auctions</Text>
                </TouchableOpacity>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md border border-gray-300`}>
                  <Text style={tw`text-sm font-bold text-gray-400`}>Paylater</Text>
                </TouchableOpacity>
            </View>
          </View>
          <View style={tw`flex gap-2 px-4 pt-4`}>
            <Text style={tw`text-[#223263] text-base font-bold`}>Item Locations</Text>
            <View style={tw`pt-3 pb-0 mb-0 flex flex-row flex-wrap gap-2 w-full`}>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md border border-gray-300`}>
                  <Text style={tw`text-sm font-bold text-gray-400`}>US Only</Text>
                </TouchableOpacity>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md bg-[#40BFFF] opacity-25`}>
                  <Text style={tw`text-sm font-bold text-opacity-100 text-sky-900`}>North America</Text>
                </TouchableOpacity>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md border border-gray-300`}>
                  <Text style={tw`text-sm font-bold text-gray-400`}>Europe</Text>
                </TouchableOpacity>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md border border-gray-300`}>
                  <Text style={tw`text-sm font-bold text-gray-400`}>Asia</Text>
                </TouchableOpacity>
            </View>
          </View>
          <View style={tw`flex gap-2 px-4 pt-4 mb-8`}>
            <Text style={tw`text-[#223263] text-base font-bold`}>Show Only</Text>
            <View style={tw`pt-3 pb-0 mb-0 flex flex-row flex-wrap gap-2 w-full mb-14`}>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md border border-gray-300`}>
                  <Text style={tw`text-sm font-bold text-gray-400`}>Free Return</Text>
                </TouchableOpacity>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md bg-[#40BFFF] opacity-25`}>
                  <Text style={tw`text-sm font-bold text-opacity-100 text-sky-900`}>Return Accepted</Text>
                </TouchableOpacity>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md border border-gray-300`}>
                  <Text style={tw`text-sm font-bold text-gray-400`}>Authorized Seller</Text>
                </TouchableOpacity>
                <TouchableOpacity style={tw`py-3 px-4 rounded-md border border-gray-300`}>
                  <Text style={tw`text-sm font-bold text-gray-400`}>Completed Items</Text>
                </TouchableOpacity>
            </View>
          </View>
      </ScrollView>
    </Animated.View>
  )
};

export default Filter;