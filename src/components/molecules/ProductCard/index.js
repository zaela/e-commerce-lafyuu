import { View, Text, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import tw from 'twrnc';
import { Gap } from '../../atoms';

const ProductCard = ({product, width, navigation}) => {
  console.log('product :', product.image)
  return (
    <TouchableOpacity style={tw`${width} p-3 rounded-md border border-[#EBF0FF]`} onPress={() => navigation.navigate('DetailProduct')}>
      {/* <Image source={{uri:product.image}}/> */}
      <Image source={require('C:/nextjs_express/lafyuu/src/assets/images/sepatu1.png')} style={tw`w-auto rounded-md`}/>
      <Gap height="h-1"/>
      <Text style={tw`text-[#223263] font-semibold text-sm`}>{product?.name}</Text>
      <Gap height="h-1"/>
      <Text style={tw`text-[#40BFFF] font-semibold text-sm`}>{product.price - (product.price * product.discount / 100)}</Text>
      <Gap height="h-1"/>
      <View style={tw`flex-row gap-3`}>
        <Text style={tw`text-[#9098B1] font-semibold text-xs`}>{product.price}</Text>
        <Text style={tw`text-[#FB7181] font-semibold text-xs`}>{product.discount}% off</Text>
      </View>
    </TouchableOpacity>
  )
}

export default ProductCard