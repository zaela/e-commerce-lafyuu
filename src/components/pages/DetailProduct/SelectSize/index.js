import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import React from 'react'
import tw from 'twrnc'

const SelectSize = () => {
  const sizes = [
    {
      number:'26',
      selected:false
    },
    {
      number:'27',
      selected:false
    },
    {
      number:'28',
      selected:false
    },
    {
      number:'29',
      selected:true
    },
    {
      number:'30',
      selected:false
    },
    {
      number:'31',
      selected:false
    },
  ]
  return (
    <View>
      <Text style={tw`text-[#223263] font-semibold text-base`}>Select Size</Text>
      <ScrollView style={tw`py-2`} horizontal showsHorizontalScrollIndicator={false}>
        {
          sizes.map((size, index) => {
            return(
              <TouchableOpacity key={index} style={tw`${size.selected ? 'border border-[#40BFFF]' : 'border border-gray-200'} rounded-full p-4 mr-3`}>
                <Text style={tw`text-[#223263] text-sm`}>{size.number}</Text>
              </TouchableOpacity>
            )
          })
        }
      </ScrollView>
    </View>
  )
}

export default SelectSize