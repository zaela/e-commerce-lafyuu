import React, { useState } from 'react';
import { ImageBackground, ScrollView, Text, View } from 'react-native';
import tw from 'twrnc';
import { BannerSlider, Filter, Gap, Header } from '../../components';
import { FlashSale, MegaSale, RecomendedProduct } from '../../components/pages';
import { Categories } from '../../components/pages/Explore';

const Explore = () => {
  const [filterOpen, setFilterOpen] = useState(false)
    const categories1 = [
        {
          name:'Man Shirt',
          link:'/',
        },
        {
          name:'Man Work Equipment',
          link:'/',
        },
        {
          name:'Man T-Shirt',
          link:'/',
        },
        {
          name:'Man SHoes',
          link:'/',
        },
        {
          name:'Man Pants',
          link:'/',
        },
        {
          name:'Man Underware',
          link:'/',
        },
    ]
    console.log('filterOpen :', filterOpen)
    return (
        <>
            <Filter open={filterOpen} setOpen={setFilterOpen} filter={() => {}} showFilter={() => {}} />
            <Header searchForm={true} filter={true} showFilter={() => setFilterOpen(true)} />
            <ScrollView horizontal={false} showsHorizontalScrollIndicator={false} style={tw`flex-1 bg-white px-4 w-full my-2`} >
                <Gap height="h-4"/>
                <Categories categories={categories1} title="Man Fashion"/>
                <Categories categories={categories1} title="Woman Fashion"/>
                <Gap height="h-4"/>
                <RecomendedProduct/>
            </ScrollView>
        </>
    )
}

export default Explore