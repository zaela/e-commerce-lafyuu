import React from 'react';
import { ImageBackground, ScrollView, Text, View } from 'react-native';
import tw from 'twrnc';
import { IcList, IcOfferOn, NotificationOn } from '../../assets';
import { BannerSlider, Category, Gap, Header } from '../../components';
import { FlashSale, MegaSale, RecomendedProduct } from '../../components/pages';
import { RowNotification } from '../../components/pages/Notification';

const NotifOffer = ({navigation}) => {
    return (
        <>
            <Header title="Notification" subTitle="Offer" onBack={() => navigation.goBack()} />
            <View style={tw`flex-1 bg-white px-4 w-full py-4`}>
                <RowNotification key={1} icon={<IcOfferOn height={28} width={28}/>} 
                    title="The best Title" navigation={() => navigation.navigate("/")}
                    offer="Culpa cillum consectetur labore nulla nulla magna irure. Id veniam culpa officia aute dolor amet deserunt ex proident commodo"
                />
                <Gap height="h-5"/>
                <RowNotification key={2} icon={<IcOfferOn height={28} width={28}/>} 
                    title="Jabuary sale 89% all product" navigation={() => navigation.navigate("/")}
                    offer="Culpa cillum consectetur labore nulla nulla magna irure. Id veniam culpa officia aute dolor amet deserunt ex proident commodo"
                />
                <Gap height="h-5"/>
                <RowNotification key={3} icon={<IcOfferOn height={28} width={28}/>} 
                    title="Special offer 33% cashback" navigation={() => navigation.navigate("/")}
                    offer="Culpa cillum consectetur labore nulla nulla magna irure. Id veniam culpa officia aute dolor amet deserunt ex proident commodo"
                />
            </View>
        </>
    )
}

export default NotifOffer