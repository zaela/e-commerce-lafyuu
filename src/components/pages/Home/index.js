import FlashSale from './FlashSale';
import MegaSale from './MegaSale';
import RecomendedProduct from './RecomendedProduct';

export {FlashSale, MegaSale, RecomendedProduct};