import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import tw from 'twrnc';

const Button = ({text, color = '#FFC700', padding = 'p-4', textColor = '#020202', onPress}) => {
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
      <View style={tw`bg-[${color}] ${padding} rounded-md`}>
        <Text style={tw`text-sm font-serif text-[${textColor}] text-center`}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Button;

