import { View, Text } from 'react-native'
import React from 'react'
import { HorizontalView, ProductCard } from '../../../molecules'

const MegaSale = () => {
  const products = [
    {
      image:'C:/nextjs_express/lafyuu/src/assets/images/sepatu1.png',
      // image:'https://dxkvlfvncvqr8.cloudfront.net/media/product-image/SM-757-BKRD-1.jpg',
      name:'FS - Nike Air Max 270 React',
      price:400000,
      discount:22,
    },
    {
      image:'C:/nextjs_express/lafyuu/src/assets/images/sepatu2.png',
      // image:'https://dxkvlfvncvqr8.cloudfront.net/media/product-image/SM-757-BKRD-1.jpg',
      name:'AS - Adidas Air 270 React',
      price:330000,
      discount:30,
    },
    {
      image:'C:/nextjs_express/lafyuu/src/assets/images/tas1.png',
      // image:'https://dxkvlfvncvqr8.cloudfront.net/media/product-image/SM-757-BKRD-1.jpg',
      name:'AS - Adidas Air 270 React',
      price:220000,
      discount:15,
    },
    {
      image:'C:/nextjs_express/lafyuu/src/assets/images/tas1.png',
      // image:'https://dxkvlfvncvqr8.cloudfront.net/media/product-image/SM-757-BKRD-1.jpg',
      name:'Tas Air 270 Luxury React',
      price:530000,
      discount:34,
    },
  ]
  return (
    <HorizontalView label="Mega Sale" directionLabel="See More">
      {
        products.length > 0 && products.map((product, index) => {
          return(
            <ProductCard width="w-36" key={index} product={product}/>
          )
        })
      }
    </HorizontalView>
  )
}

export default MegaSale