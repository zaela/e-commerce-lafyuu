import React from 'react';
import { ImageBackground, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import tw from 'twrnc';
import { BannerSlider, Category, Gap, Header, LabelValue, Button } from '../../components';
import { FlashSale, MegaSale, RecomendedProduct } from '../../components/pages';
import { RowOrder } from '../../components/pages/Order';

const Cart = ({navigation}) => {
    return (
        <View style={tw`flex-1`}>
            <Header title="Your Cart" />
            <ScrollView horizontal={false} showsHorizontalScrollIndicator={false} style={tw`bg-white px-4 w-full`} >
                <Gap height="h-4"/>
                <RowOrder order={{name:'Nike Air Zoom Pegasus 36 Miami', price:'230000'}}/>
                <Gap height="h-4"/>
                <RowOrder order={{name:'Nike Air Zoom Pegasus 36 New York', price:'235000'}}/>
                <Gap height="h-4"/>
                <View style={tw`border border-[#9098B1] focus:border-[#40BFFF] rounded-md pt-[0.05rem] pb-[0.05rem] flex-row items-center justify-between w-full`}>
                    <TextInput
                        style={tw`ml-2`}
                        placeholder="Enter Cupon Code"
                        name="cuponCode"
                        value=''
                    />
                    <TouchableOpacity style={tw`bg-[#40BFFF] h-full w-20 flex-row justify-center items-center`}>
                        <Text style={tw`text-white`}>Apply</Text>
                    </TouchableOpacity>
                </View>
                <Gap height="h-4"/>
                <LabelValue label="Items (3)" value="598.86"/>
                <Gap height="h-2"/>
                <LabelValue label="Shipping" value="22.86"/>
                <Gap height="h-2"/>
                <LabelValue label="Import Charges" value="21.86"/>
                <Gap height="h-4"/>
                <LabelValue label="Total Price" labelStyle="text-[#223263] font-semibold" value="21.86" valueStyle="text-[#40BFFF] font-semibold"/>
                <Gap height="h-8"/>
                <View style={tw`flex-1 justify-end pb-4`}>
                    <Button text="Check Out" padding='p-4' color='#40BFFF' textColor='#ffff' onPress={() => {navigation.navigate('ShipTo')}} />
                </View>
            </ScrollView>
        </View>
    )
}

export default Cart