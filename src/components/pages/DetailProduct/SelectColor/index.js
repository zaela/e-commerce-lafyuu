import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import React from 'react'
import tw from 'twrnc'

const SelectColor = () => {
  const colors = [
    {
      color:'FFC833',
      selected:true
    },
    {
      color:'40BFFF',
      selected:false
    },
    {
      color:'FB7181',
      selected:false
    },
    {
      color:'53D1B6',
      selected:false
    },
    {
      color:'5C61F4',
      selected:false
    },
    {
      color:'223263',
      selected:false
    },
  ]
  return (
    <View>
      <Text style={tw`text-[#223263] font-semibold text-base`}>Select Color</Text>
      <ScrollView style={tw`py-2`} horizontal showsHorizontalScrollIndicator={false}>
        {
          colors.map((color, index) => {
            return(
              <TouchableOpacity key={index} style={tw`bg-[#${color.color}] rounded-full p-4 mr-3`}>
                {/* <Text style={tw`text-[#223263] text-sm`}>{color.color}</Text> */}
                  <View style={tw`h-4 w-4 rounded-full ${color.selected ? 'bg-white' : ''}`}/>
              </TouchableOpacity>
            )
          })
        }
      </ScrollView>
    </View>
  )
}

export default SelectColor