import React from 'react';
import { ImageBackground, ScrollView, Text, View } from 'react-native';
import tw from 'twrnc';
import { IcList, IcOfferOn, IcTransaction, NotificationOn } from '../../assets';
import { BannerSlider, Category, Gap, Header } from '../../components';
import { FlashSale, MegaSale, RecomendedProduct } from '../../components/pages';
import { RowNotification } from '../../components/pages/Notification';

const NotifActivity = ({navigation}) => {
    return (
        <>
            <Header title="Notification" subTitle="Activity" onBack={() => navigation.goBack()} />
            <View style={tw`flex-1 bg-white px-4 w-full py-4`}>
                <RowNotification key={1} icon={<IcTransaction height={26} width={26}/>} 
                    title="Transaction Nike Air Zoom Product" navigation={() => navigation.navigate("/")}
                    activity="Culpa cillum consectetur labore nulla nulla magna irure. Id veniam culpa officia"
                />
                <Gap height="h-5"/>
                <RowNotification key={2} icon={<IcTransaction height={26} width={26}/>} 
                    title="Transaction Nike Air Zoom Pegasus 36 Miami" navigation={() => navigation.navigate("/")}
                    activity="Culpa cillum consectetur labore nulla nulla magna irure deserunt ex proident commodo"
                />
                <Gap height="h-5"/>
                <RowNotification key={3} icon={<IcTransaction height={26} width={26}/>} 
                    title="Transaction Nike Air Max" navigation={() => navigation.navigate("/")}
                    activity="Culpa cillum veniam culpa officia aute dolor amet deserunt ex proident commodo"
                /> 
            </View>
        </>
    )
}

export default NotifActivity