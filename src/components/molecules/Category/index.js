import { View, Text, ScrollView, Image } from 'react-native';
import React from 'react';
import tw from 'twrnc';
import { Gap } from '../../atoms';

const Category = () => {
  return (
    <View>
      <View style={tw`flex-row justify-between`}>
        <Text style={tw`text-[#223263] text-base font-bold`}>Category</Text>
        <Text style={tw`text-[#40BFFF] text-base font-bold`}>More Category</Text>
      </View>
      <Gap height="h-3"/>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <View style={tw`flex items-center gap-2 w-26 py-5`}>
            <Image source={require('C:/nextjs_express/lafyuu/src/assets/productIcon/dress.png')} style={tw`h-7 w-7`}/>
            <Text style={tw`text-center`}>Dress</Text>
        </View>
        <View style={tw`flex items-center gap-2 w-26 py-5`}>
            <Image source={require('C:/nextjs_express/lafyuu/src/assets/productIcon/shirt.png')} style={tw`h-7 w-7`}/>
            <Text style={tw`text-center`}>Man Shirt</Text>
        </View>
        <View style={tw`flex items-center gap-2 w-26 py-5`}>
            <Image source={require('C:/nextjs_express/lafyuu/src/assets/productIcon/manBag.png')} style={tw`h-7 w-7`}/>
            <Text style={tw`text-center`}>Man Bag</Text>
        </View>
        <View style={tw`flex items-center gap-2 w-26 py-5`}>
            <Image source={require('C:/nextjs_express/lafyuu/src/assets/productIcon/manShoes.png')} style={tw`h-7 w-7`}/>
            <Text style={tw`text-center`}>Man Shoes</Text>
        </View>
        <View style={tw`flex items-center gap-2 w-26 py-5`}>
            <Image source={require('C:/nextjs_express/lafyuu/src/assets/productIcon/womanBag.png')} style={tw`h-7 w-7`}/>
            <Text style={tw`text-center`}>Woman Bag</Text>
        </View>
      </ScrollView>
    </View>
  )
}

export default Category