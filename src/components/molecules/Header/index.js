import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {BlueSearch, IcBack, IcFilter, IcSort, Love, Notification} from '../../../assets';
import tw from 'twrnc';
import { TextInput } from '../../atoms';

const Header = ({title, subTitle, onBack, searchForm, filter, showFilter, navigation, borderBottom}) => {
  if (searchForm) {
    return(
      <View style={tw`bg-white px-[1.2rem] pt-[0.9rem] pb-[0.9rem] flex-row w-full flex-row`}>
        <View style={tw`w-4/5`}>
          <TextInput
            placeholder="Search Product"
            icon={<BlueSearch style={tw`px-4`}/>}
            value=''
          />
        </View>
        <View style={tw`w-1/5 flex-row justify-end items-center gap-2`}>
          {
            filter ? (
              <>
              <TouchableOpacity>
                <IcSort height={25} width={25}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => showFilter()}>
                <IcFilter height={25} width={25}/>
              </TouchableOpacity>
              </>
            ) : (
              <>
              <TouchableOpacity>
                <Love height={30} width={30}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate('Notification')}>
                <Notification height={25} width={25}/>
              </TouchableOpacity>
              </>
            )
          }
        </View>
      </View>
    )
  }else{
    return (
      <View style={tw`bg-white px-[1.2rem] pt-[0.9rem] pb-[0.9rem] flex-row ${borderBottom && 'border-b border-gray-200'}`}>
        {onBack && (
          <TouchableOpacity activeOpacity={0.7} onPress={onBack}>
            <View style={tw`p-2 mr-4 ml-[-5]`}>
              <IcBack />
            </View>
          </TouchableOpacity>
        )}
        <View style={tw`flex flex-col justify-center`}>
          <Text style={tw`text-[1.2rem] text-[#020202]`}>{title}</Text>
          {
            subTitle && (
              <Text style={tw`text-[0.9rem] text-[#8D92A3]`}>{subTitle}</Text>
            )
          }
        </View>
      </View>
    );
  }
};

export default Header;