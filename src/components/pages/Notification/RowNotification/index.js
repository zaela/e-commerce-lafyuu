import { View, Text, Image, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import tw from 'twrnc'
import { IcList } from '../../../../assets'

const RowNotification = ({title, icon, feed, offer, activity, number, navigation}) => {
  const [bgColor, setBgColor] = useState('bg-white')

  return (
    <TouchableOpacity 
      style={tw`flex-row gap-3 p-4 px-4 ${bgColor}`}
      onPressIn={() => setBgColor('bg-[#EBF0FF]')}
      onPressOut={() => setBgColor('bg-white')}
      onPress={() => {
        navigation()
      }}
    >
      <View>
        {
            feed ? (
                <Image style={tw`h-13 w-13`} source={require('C:/nextjs_express/lafyuu/src/assets/images/sepatu1.png')}/>
            ):(
                icon
            )
        }
      </View>
      <View style={tw`flex-row justify-between flex-1`}>
        <View>
          <Text style={tw`text-[#223263] text-base font-bold`}>
              {title}
          </Text>
          {
              feed && (
                  <View>
                      <Text style={tw`text-gray-400 text-sm font-bold`}>
                      {feed ? feed : '-'}
                      </Text>
                      <Text style={tw`text-gray-500 text-sm font-bold mt-1`}>April 30, 2014 1:01 PM</Text>
                  </View>
              )
          }
          {
              offer && (
                  <View>
                      <Text style={tw`text-gray-400 text-sm font-bold`}>
                      {offer ? offer : '-'}
                      </Text>
                      <Text style={tw`text-gray-400 text-sm font-bold mt-1`}>April 30, 2014 1:01 PM</Text>
                  </View>
              )
          }
          {
              activity && (
                  <View>
                      <Text style={tw`text-gray-400 text-sm font-bold`}>
                      {activity ? activity : '-'}
                      </Text>
                      <Text style={tw`text-gray-500 text-sm font-bold mt-1`}>April 30, 2014 1:01 PM</Text>
                  </View>
              )
          }
        </View>
        {
          number && (
            <View style={tw`bg-[#FB7181] w-8 h-8 p-1 rounded-full flex items-center justify-center`}>
              <Text style={tw`text-white text-[0.7rem]`}>{number}</Text>
            </View>
          )
        }
      </View>
    </TouchableOpacity>
  )
}

export default RowNotification