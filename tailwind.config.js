module.exports = {
    theme: {
      extend: {
        zIndex: {
          '100': '100',
          '1000': '1000',
          '3000': '3000',
        }
      }
    }
  }