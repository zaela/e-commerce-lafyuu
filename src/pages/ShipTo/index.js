import React from 'react';
import { ScrollView, View } from 'react-native';
import tw from 'twrnc';
import { Button, Gap, Header } from '../../components';
import { Address } from '../../components/pages/ShipTo';

const ShipTo = ({navigation}) => {
  return (
    <View style={tw`flex-1`}>
        <Header onBack={() => navigation.goBack()} title="Ship To" borderBottom/>
        <ScrollView horizontal={false} showsHorizontalScrollIndicator={false} style={tw`flex flex-col gap-4 bg-white px-4 w-full `} >
          <Gap height="h-4"/>
          <Address active={true} onPress={() => navigation.navigate('EditAddress')} />
          <Gap height="h-4"/>
          <Address active={false} onPress={() => navigation.navigate('EditAddress')} />
          <Gap height="h-8"/>
          <View style={tw`flex-1 justify-end pb-4`}>
              <Button text="Next" padding='p-4' color='#40BFFF' textColor='#ffff' onPress={() => {navigation.navigate('Payment')}} />
          </View>
        </ScrollView>    
    </View>
  )
}

export default ShipTo