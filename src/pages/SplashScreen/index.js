import { View, Text } from 'react-native'
import React, {useEffect} from 'react';
import tw from 'twrnc'
import { Logo } from '../../assets'

const SplashScreen = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
          navigation.replace('SignIn');
        }, 2000);
    }, []);

    return (
        <View style={tw`flex-1 items-center justify-center bg-[#40BFFF]`}>
            <Logo/>
        </View>
    )
}

export default SplashScreen