import React from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import tw from 'twrnc';
import { Love } from '../../assets';
import { Button, Category, Gap, Header, HorizontalView, ImageSlider, ProductCard, Rating } from '../../components';
import { RecomendedProduct } from '../../components/pages';
import { ReviewProduct, SelectColor, SelectSize, Specification } from '../../components/pages/DetailProduct';

const DetailProduct = ({navigation}) => {
    const products = [
        {
          image:'C:/nextjs_express/lafyuu/src/assets/images/sepatu1.png',
          // image:'https://dxkvlfvncvqr8.cloudfront.net/media/product-image/SM-757-BKRD-1.jpg',
          name:'FS - Nike Air Max 270 React',
          price:400000,
          discount:22,
        },
        {
          image:'C:/nextjs_express/lafyuu/src/assets/images/sepatu2.png',
          // image:'https://dxkvlfvncvqr8.cloudfront.net/media/product-image/SM-757-BKRD-1.jpg',
          name:'AS - Adidas Air 270 React',
          price:330000,
          discount:30,
        },
        {
          image:'C:/nextjs_express/lafyuu/src/assets/images/tas1.png',
          // image:'https://dxkvlfvncvqr8.cloudfront.net/media/product-image/SM-757-BKRD-1.jpg',
          name:'AS - Adidas Air 270 React',
          price:220000,
          discount:15,
        },
        {
          image:'C:/nextjs_express/lafyuu/src/assets/images/tas1.png',
          // image:'https://dxkvlfvncvqr8.cloudfront.net/media/product-image/SM-757-BKRD-1.jpg',
          name:'Tas Air 270 Luxury React',
          price:530000,
          discount:34,
        },
      ]
    return (
        <>
            <Header onBack={() => navigation.goBack()} title="Detail Product"/>
            {/* <View style={tw`flex-1 bg-white px-4 w-full my-2`}> */}
            <ScrollView horizontal={false} showsHorizontalScrollIndicator={false} style={tw`flex-1 bg-white px-4 w-full`} >
                <Gap height="h-4"/>
                <ImageSlider/>
                <Gap height="h-1"/>
                <View style={tw`flex-row justify-between`}>
                    <Text style={tw`text-[#223263] font-semibold text-xl`}>Nike Air Zoom Pegasus 36 Miami</Text>
                    <TouchableOpacity>
                        <Love height={23} width={23}/>
                    </TouchableOpacity>
                </View>
                <Gap height="h-1"/>
                <Text style={tw`text-[#40BFFF] font-semibold text-xl`}>234000</Text>
                <Gap height="h-1"/>
                <Rating rate={4}/>
                <Gap height="h-4"/>
                <SelectSize/>
                <Gap height="h-4"/>
                <SelectColor/>
                <Gap height="h-4"/>
                <Specification/>
                <Gap height="h-6"/>
                <ReviewProduct/>
                <Gap height="h-4"/>
                <HorizontalView label="You Might Also Like">
                {
                    products.length > 0 && products.map((product, index) => {
                    return(
                        <ProductCard width="w-36" key={index} product={product}/>
                    )
                    })
                }
                </HorizontalView>
                <Gap height="h-5"/>
                <Button text="Add To Cart" padding='p-4' color='#40BFFF' textColor='#ffff' onPress={() => navigation.navigate('Home')} />
                <Gap height="h-5"/>

            </ScrollView>
            {/* </View> */}
        </>
    )
}

export default DetailProduct