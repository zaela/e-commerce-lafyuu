import { PromotionImage1, PromotionImage2 } from "./images";

export const carousels = [
    {
        "id": 1,
        "title": "Super Flash Sale",
        "promo": "50% off",
        "image": "C:/nextjs_express/lafyuu/src/assets/images/promotionImage1.png",
        "expire": {"hour":"02", "minute":"33", "second":"40"}
    },
    {
        "id": 2,
        "title": "Yellow Shoe",
        "promo": "50% off",
        "image": "C:/nextjs_express/lafyuu/src/assets/images/promotionImage2.png",
        "expire": {"hour":"04", "minute":"50", "second":"56"}
    },
]