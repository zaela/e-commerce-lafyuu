import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import { Facebook, Google, LogoBlue, Message, Password, User } from '../../assets'
import tw from 'twrnc'
import { Button, Gap, TextInput } from '../../components'

const SignUp = ({navigation}) => {
  return (
    <View>
      <View style={tw`flex items-center justify-center bg-white mt-30`}>
        <LogoBlue/>
        <Gap height='h-4'/>
        <Text style={tw`text-[#223263] font-bold text-base font-serif tracking-wide`}>Let's Get Started</Text>
        <Gap height='h-1'/>
        <Text style={tw`text-[#9098B1] font-bold text-xs font-serif tracking-wide`}>Create a new account</Text>
        <Gap height='h-4'/>
        <View style={tw`w-full px-4`}>
          <TextInput
            placeholder="Full Name"
            icon={<User style={tw`px-4`}/>}
            value=''
            // onChangeText={(value) => setForm('email', value)}
          />
          <Gap height='h-3' />
          <TextInput
            placeholder="Your Email"
            icon={<Message style={tw`px-4`}/>}
            value=''
            // onChangeText={(value) => setForm('email', value)}
          />
          <Gap height='h-3' />
          <TextInput
            placeholder="Password"
            icon={<Password style={tw`px-4`}/>}
            value=''
            // onChangeText={(value) => setForm('password', value)}
            secureTextEntry
          />
          <Gap height='h-3' />
          <TextInput
            placeholder="Confirm Password"
            icon={<Password style={tw`px-4`}/>}
            value=''
            // onChangeText={(value) => setForm('password', value)}
            secureTextEntry
          />
          <Gap height='h-4' />
          <Button text="Sign Up" padding='p-4' color='#40BFFF' textColor='#ffff' onPress={() => {}} />
          <Gap height='h-6' />

          <View style={tw`flex-row justify-center`}>
            <View style={tw`flex-row`}>
              <Text style={tw`text-[#9098B1] text-sm text-center`}>Have a account ?</Text>
              <TouchableOpacity activeOpacity={0.7} onPress={() => navigation.navigate('SignIn')}>
                <Text style={tw`text-[#40BFFF] text-sm font-semibold text-center`}> Sign In</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  )
}

export default SignUp