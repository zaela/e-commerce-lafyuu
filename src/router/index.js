import { createStackNavigator } from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import {
    SplashScreen,
    SignIn,
    SignUp,
    Home,
    Explore,
    Cart,
    Offer,
    Account,
    DetailProduct,
    Notification,
    NotifOffer,
    NotifFeed,
    NotifActivity,
    ShipTo,
    Payment,
    ChooseCard,
    EditAddress,
    Profile,
} from '../pages';
import { BottomNavigator } from '../components';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator tabBar={(props) => <BottomNavigator {...props} />}>
      <Tab.Screen name="Home" options={{ headerShown: false }} component={Home} />
      <Tab.Screen name="Explore" options={{ headerShown: false }} component={Explore} />
      <Tab.Screen name="Cart" options={{ headerShown: false }} component={Cart} />
      <Tab.Screen name="Offer" options={{ headerShown: false }} component={Offer} />
      <Tab.Screen name="Account" options={{ headerShown: false }} component={Account} />
    </Tab.Navigator>
  );
};

const Router = () => {
  return (
    <Stack.Navigator>
      {/* <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SignIn"
        component={SignIn}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SignUp"
        component={SignUp}
        options={{headerShown: false}}
      /> */}
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="DetailProduct"
        component={DetailProduct}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Notification"
        component={Notification}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="NotifOffer"
        component={NotifOffer}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="NotifFeed"
        component={NotifFeed}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="NotifActivity"
        component={NotifActivity}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ShipTo"
        component={ShipTo}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Payment"
        component={Payment}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ChooseCard"
        component={ChooseCard}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="EditAddress"
        component={EditAddress}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Router;
