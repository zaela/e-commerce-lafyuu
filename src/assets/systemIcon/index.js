import Facebook from './facebook.svg';
import Google from './google.svg';
import Logo from './logo.svg';
import LogoBlue from './logoBlue.svg';
import Love from './love.svg';
import LoveOn from './loveOn.svg';
import Message from './message.svg';
import Notification from './notification.svg';
import NotificationOn from './notificationOn.svg';
import Password from './password.svg';
import User from './user.svg';
import Search from './search.svg';
import BlueSearch from './blueSearch.svg';
import IcBack from './backIcon.svg';
import IcHomeOn from './homeOn.svg';
import IcHomeOff from './homeOff.svg';
import IcExploreOn from './exploreOn.svg';
import IcExploreOff from './exploreOff.svg';
import IcCartOn from './cartOn.svg';
import IcCartOff from './cartOff.svg';
import IcOfferOn from './offerOn.svg';
import IcOfferOff from './offerOff.svg';
import IcAccountOn from './accountOn.svg';
import IcAccountOff from './accountOff.svg';
import IcArrowBottom from './arrowBottom.svg';
import IcFilter from './filterIcon.svg';
import IcSort from './sortIcon.svg';
import IcClose from './close.svg';
import IcStarOn from './starOn.svg';
import IcStarOff from './starOff.svg';
import IcList from './list.svg';
import IcTransaction from './transaction.svg';
import IcPlus from './plus.svg';
import IcMinus from './minus.svg';
import IcTrash from './trash.svg';
import IcBank from './bank.svg';
import IcCreditCard from './creditCard.svg';
import IcPaypal from './paypal.svg';
import IcAlertIcon from './alertIcon.svg';
import IcBag from './bag.svg';
import IcDate from './date.svg';
import IcGender from './gender.svg';
import IcLocation from './location.svg';
import IcMessage from './message.svg';
import IcPhone from './phone.svg';
import IcArrowRight from './arrowRight.svg';

export {
    Facebook,
    Google,
    Logo,
    LogoBlue,
    Love,
    Message,
    Notification,
    Password,
    User,
    Search,
    BlueSearch,
    IcBack,
    IcHomeOff,
    IcHomeOn,
    IcExploreOn,
    IcExploreOff,
    IcCartOn,
    IcCartOff,
    IcOfferOn,
    IcOfferOff,
    IcAccountOn,
    IcAccountOff,
    IcArrowBottom,
    IcFilter,
    IcSort,
    IcClose,
    IcStarOn,
    IcStarOff,
    IcList,
    NotificationOn,
    IcTransaction,
    IcPlus,
    IcMinus,
    LoveOn,
    IcTrash,
    IcBank,
    IcCreditCard,
    IcPaypal,
    IcAlertIcon,
    IcBag,
    IcDate,
    IcGender,
    IcLocation,
    IcMessage,
    IcPhone,
    IcArrowRight,
};