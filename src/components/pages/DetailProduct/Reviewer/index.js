import { View, Text, Image } from 'react-native'
import React from 'react'
import { Gap, Rating } from '../../../atoms'
import tw from 'twrnc'

const Reviewer = () => {
  return (
    <View>
        <View style={tw`flex-row items-center gap-2`}>
          <Image style={tw`rounded-full h-8 w-8`} source={require('C:/nextjs_express/lafyuu/src/assets/images/avatar.png')} />
          <View>
            <Text style={tw`text-[#223263] text-base font-bold`}>James Lawson</Text>
            <Rating rate={4}/>
          </View>
        </View>
    </View>
  )
}

export default Reviewer