import { View, Text, ScrollView, Image, FlatList } from 'react-native';
import React from 'react';
import tw from 'twrnc';
import { Gap } from '../../../atoms';

const Categories = ({categories, title}) => {
  const  Category = ({category}) => {
    return (
      <View style={tw`flex  items-center gap-2 w-22 py-5`}>
        <Image source={require('C:/nextjs_express/lafyuu/src/assets/productIcon/manBag.png')} style={tw`h-7 w-7`}/>
        <Text style={tw`text-center text-xs`}>{category.name}</Text>
      </View>
    )
  }
  return (
    <View>
      <View style={tw`flex-row justify-between`}>
        <Text style={tw`text-[#223263] text-base font-bold`}>{title}</Text>
      </View>
      <Gap height="h-3"/>
      <FlatList
        data={categories}
        renderItem={({item}) => (
          <Category category={item}/>
        )}
        //Setting the number of column
        numColumns={4}
        columnWrapperStyle={tw`flex flex-row my-2`}
        showsVerticalScrollIndicator={false}
        scrollEnabled={false}
        keyExtractor={(item, index) => index}
      />
    </View>
  )
}

export default Categories