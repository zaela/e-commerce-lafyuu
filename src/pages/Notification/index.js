import React from 'react';
import { ImageBackground, ScrollView, Text, View } from 'react-native';
import tw from 'twrnc';
import { IcList, IcOfferOn, NotificationOn } from '../../assets';
import { BannerSlider, Category, Gap, Header } from '../../components';
import { FlashSale, MegaSale, RecomendedProduct } from '../../components/pages';
import { RowNotification } from '../../components/pages/Notification';

const Notification = ({navigation}) => {
    return (
        <>
            <Header title="Notification" onBack={() => navigation.goBack()} />
            <View style={tw`flex-1 bg-white w-full py-4`}>
                <RowNotification key={1} icon={<IcOfferOn height={28} width={28}/>} title="Offer" number={4} navigation={() => navigation.navigate("NotifOffer")}/>
                <RowNotification key={2} icon={<IcList height={28} width={28}/>} title="Feed" number={233} navigation={() => navigation.navigate("NotifFeed")}/>
                <RowNotification key={3} icon={<NotificationOn height={28} width={28}/>} title="Activity" number={2} navigation={() => navigation.navigate("NotifActivity")}/>
            </View>
        </>
    )
}

export default Notification