import React from 'react';
import { ImageBackground, ScrollView, Text, View } from 'react-native';
import tw from 'twrnc';
import { BannerSlider, Category, Gap, Header } from '../../components';
import { FlashSale, MegaSale, RecomendedProduct } from '../../components/pages';

const Home = ({navigation}) => {
    return (
        <>
            <Header searchForm={true} navigation={navigation} />
            {/* <View style={tw`flex-1 bg-white px-4 w-full my-2`}> */}
            <ScrollView horizontal={false} showsHorizontalScrollIndicator={false} style={tw`flex-1 bg-white px-4 w-full my-2`} >
                <BannerSlider/>
                <Gap height="h-4"/>
                <Category/>
                <Gap height="h-4"/>
                <FlashSale/>
                <Gap height="h-4"/>
                <MegaSale/>
                <Gap height="h-4"/>
                <ImageBackground  source={require('C:/nextjs_express/lafyuu/src/assets/images/recomendedProduct.png')}>
                    <View style={tw`h-46 w-full px-5 py-6 rounded-md flex flex-col justify-center`}>
                        <View>
                            <Text style={tw`text-white text-2xl font-bold`}>Recomended</Text>
                            <Text style={tw`text-white text-2xl font-bold`}>Product</Text>
                            <Gap height="h-5"/>
                            <Text style={tw`text-white text-sm font-bold`}>We recommend the best for you</Text>
                        </View>
                    </View>
                </ImageBackground>
                <Gap height="h-4"/>
                <RecomendedProduct navigation={navigation}/>
            </ScrollView>
            {/* </View> */}
        </>
    )
}

export default Home