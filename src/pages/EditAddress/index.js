import { View, Text, TouchableOpacity, ScrollView } from 'react-native'
import React from 'react'
import { Facebook, Google, LogoBlue, Message, Password } from '../../assets'
import tw from 'twrnc'
import { Button, Gap, Header, TextInput } from '../../components'

const EditAddress = ({navigation}) => {
  return (
    <View style={tw`flex-1 bg-white`}>
      <Header onBack={() => navigation.goBack()} title="Edit Address" borderBottom/>
      <Gap height='h-4'/>
      <ScrollView style={tw`w-full px-4`}>
        <TextInput
          label="Country or region"
          placeholder="United State"
          value=''
          // onChangeText={(value) => setForm('email', value)}
        />
        <Gap height='h-3' />
        <TextInput
          label="First Name"
          placeholder=""
          value=''
          // onChangeText={(value) => setForm('password', value)}
        />
        <Gap height='h-3' />
        <TextInput
          label="Last Name"
          placeholder=""
          value=''
          // onChangeText={(value) => setForm('password', value)}
        />
        <Gap height='h-3' />
        <TextInput
          label="Street Address"
          placeholder=""
          value=''
          // onChangeText={(value) => setForm('password', value)}
        />
        <Gap height='h-3' />
        <TextInput
          label="City"
          placeholder=""
          value=''
          // onChangeText={(value) => setForm('password', value)}
        />
        <Gap height='h-3' />
        <TextInput
          label="Province"
          placeholder=""
          value=''
          // onChangeText={(value) => setForm('password', value)}
        />
        <Gap height='h-3' />
        <TextInput
          label="Zip Code"
          placeholder=""
          value=''
          // onChangeText={(value) => setForm('password', value)}
        />
        <Gap height='h-3' />
        <TextInput
          label="Phone Number"
          placeholder=""
          value=''
          // onChangeText={(value) => setForm('password', value)}
        />
        <Gap height='h-4' />
        <Button text="Edit Address" padding='p-4' color='#40BFFF' textColor='#ffff' onPress={() => navigation.navigate('Home')} />
        <Gap height='h-4' />

      </ScrollView>
    </View>
  )
}

export default EditAddress