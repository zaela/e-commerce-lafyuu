import React from 'react';
import {Text} from 'react-native';
import currencyConverter from '../../../helpers/currencyConverter';

const NumberFormat = ({number, type, style}) => {
  if (type === 'decimal') {
    return (
      <Text>{currencyConverter(number)}</Text>
    );
  }
  return (
    <Text>{currencyConverter(number)}</Text>
  );
};

export default NumberFormat;
