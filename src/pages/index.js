import SplashScreen from './SplashScreen';
import SignIn from './SignIn';
import SignUp from './SignUp';
import Home from './Home';
import Explore from './Explore';
import Cart from './Cart';
import Offer from './Offer';
import Account from './Account';
import DetailProduct from './DetailProduct';
import Notification from './Notification';
import NotifOffer from './NotifOffer';
import NotifFeed from './NotifFeed';
import NotifActivity from './NotifActivity';
import ShipTo from './ShipTo';
import Payment from './Payment';
import ChooseCard from './ChooseCard';
import EditAddress from './EditAddress';
import Profile from './Profile';


export {
  SplashScreen,
  SignIn,
  SignUp,
  Home,
  Explore,
  Cart,
  Offer,
  Account,
  DetailProduct,
  Notification,
  NotifOffer,
  NotifFeed,
  NotifActivity,
  ShipTo,
  Payment,
  ChooseCard,
  EditAddress,
  Profile,
};
