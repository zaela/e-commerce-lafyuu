import { View, Text } from 'react-native'
import React from 'react'
import tw from 'twrnc'
import { IcStarOff, IcStarOn } from '../../../assets'

const Rating = ({rate, rateNumber, rater}) => {
    console.log('rateNumber :', rateNumber)
    console.log('rater :', rater)
    const Stars = ({rate}) => {
        switch (rate) {
            case 1:
                return (
                    <>
                        <IcStarOn/>
                        <IcStarOff/>
                        <IcStarOff/>
                        <IcStarOff/>
                        <IcStarOff/>
                    </>
                )
            case 2:
                return (
                    <>
                        <IcStarOn/>
                        <IcStarOn/>
                        <IcStarOff/>
                        <IcStarOff/>
                        <IcStarOff/>
                    </>
                )
            case 3:
                return (
                    <>
                        <IcStarOn/>
                        <IcStarOn/>
                        <IcStarOn/>
                        <IcStarOff/>
                        <IcStarOff/>
                    </>
                )
            case 4:
                return (
                    <>
                        <IcStarOn/>
                        <IcStarOn/>
                        <IcStarOn/>
                        <IcStarOn/>
                        <IcStarOff/>
                    </>
                )
            case 5:
                return (
                    <>
                        <IcStarOn/>
                        <IcStarOn/>
                        <IcStarOn/>
                        <IcStarOn/>
                        <IcStarOn/>
                    </>
                )
                break;
        
            default:
                return (
                    <>
                        <IcStarOff/>
                        <IcStarOff/>
                        <IcStarOff/>
                        <IcStarOff/>
                        <IcStarOff/>
                    </>
                )
                break;
        }
    }
    return (
        <View style={tw`flex-row items-center gap-3`}>
            <Stars rate={rate} />
            {
                rateNumber && (
                    <Text style={tw`text-gray-400 font-semibold text-sm`}>{rateNumber}</Text>
                )
            }
            {
                rater && (
                    <Text style={tw`text-gray-400 text-sm`}>({rater} Review)</Text>
                )
            }
        </View>
    )
}

export default Rating