import { View, Text, ScrollView } from 'react-native'
import React from 'react'
import tw from 'twrnc'
import { Gap, Header } from '../../components'
import { IcBank, IcCreditCard, IcPaypal } from '../../assets'
// IcBank,
//     IcCreditCard,
//     IcPaypal,
const Payment = ({navigation}) => {
  return (
    <View style={tw`flex-1`}>
        <Header onBack={() => navigation.goBack()} title="Payment" borderBottom/>
        <ScrollView horizontal={false} showsHorizontalScrollIndicator={false} style={tw`flex flex-col gap-4 bg-white px-4 w-full `} >
          <Gap height="h-4"/>
          <View style={tw`flex-row gap-3 pb-4 items-center`}>
              <IcCreditCard height={27} width={27}/>
              <Text style={tw`text-sm text-[#223263]`}>Credit Card or Debit</Text>
          </View>
          <View style={tw`flex-row gap-3 pb-4 items-center`}>
              <IcPaypal height={27} width={27}/>
              <Text style={tw`text-sm text-[#223263]`}>Paypal</Text>
          </View>
          <View style={tw`flex-row gap-3 pb-4 items-center`}>
              <IcBank height={27} width={27}/>
              <Text style={tw`text-sm text-[#223263]`}>Bank Transfer</Text>
          </View>
        </ScrollView>    
    </View>
  )
}

export default Payment