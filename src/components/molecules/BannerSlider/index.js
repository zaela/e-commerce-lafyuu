import { View, Text, FlatList, TouchableOpacity, Image, ImageBackground, Dimensions } from 'react-native'
import React, { useRef, useState } from 'react'
import tw from 'twrnc'
import { carousels } from '../../../assets/carousel'
import { PromotionImage1 } from '../../../assets'
import { Gap } from '../../atoms'
const viewCofigRef = {viewAreaCoveragePercentThreshold:95}
const {height, width} = Dimensions.get('window')

const BannerSlider = () => {
    let flatListRef = useRef(null)
    const [currentIndex, setCurrentIndex] = useState(0)

    const onViewRef = useRef(({changed}) => {
        if(changed[0].isViewable){
            setCurrentIndex(changed[0].index)
        }
    })

    const scrollToIndex = (index) => {
        flatListRef.current?.scrollToIndex({animated:true, index:index})
    }

    const RenderItems = ({image, title, promo, expire}) => {
        return (
            <TouchableOpacity style={{width:width, backgroundColor:'red'}} activeOpacity={1} onPress={() => console.log('clicked')}>
                <ImageBackground  source={require('C:/nextjs_express/lafyuu/src/assets/images/promotionImage1.png')}>
                    <View style={tw`h-46 w-full px-5 py-6 rounded-md`}>
                        <Text style={tw`text-white text-2xl font-bold`}>{title}</Text>
                        <Text style={tw`text-white text-2xl font-bold`}>{promo}</Text>
                        <Gap height="h-5"/>
                        <View style={tw`flex-row gap-3`}>
                            <View style={tw`p-1 bg-white rounded-sm`}>
                                <Text style={tw`text-base font-semibold text-[#223263]`}>{expire.hour}</Text>
                            </View>
                            <View style={tw`p-1 bg-white rounded-sm`}>
                                <Text style={tw`text-base font-semibold text-[#223263]`}>{expire.minute}</Text>
                            </View>
                            <View style={tw`p-1 bg-white rounded-sm`}>
                                <Text style={tw`text-base font-semibold text-[#223263]`}>{expire.second}</Text>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        )
    }
    return (
        <View style={tw`flex justify-center`}>
            <FlatList 
                data={carousels} 
                renderItem={({item}) => <RenderItems image={item.image} title={item.title} promo={item.promo} expire={item.expire} />} 
                keyExtractor={(item) => item.id+item.toString()}
                horizontal
                showsHorizontalScrollIndicator={false}
                pagingEnabled
                ref={(ref) => {
                    flatListRef.current = ref;
                }}
                viewabilityConfig={viewCofigRef}
                onViewableItemsChanged={onViewRef.current}
            />
            <View style={tw`w-full flex-row justify-center my-2`}>
                <View style={tw`flex-row gap-1`}>
                    {
                        carousels.map((carousel, index) => {
                            return(
                                <TouchableOpacity 
                                    key={index} style={tw`w-2 h-2 rounded-xl ${index === currentIndex ? 'bg-[#40BFFF]':'bg-gray-300'}`}
                                    onPress={() => scrollToIndex(index)}
                                />
                            )
                        })
                    }
                </View>
            </View>
        </View>
    )
}

export default BannerSlider