import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import tw from 'twrnc'
import { Button } from '../../../atoms'
import { IcTrash } from '../../../../assets'

const Address = ({navigation, active, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={tw`border ${active ? 'border-[#40BFFF]' : 'border-gray-200'} rounded-md p-6`}>
        <Text style={tw`text-sm text-[#223263]`}>Priscekila</Text>
        <Text style={tw`text-sm text-gray-400 mt-3`}>3711 Spring Hill Rd undefined Tallahassee, Nevada 52874 United States</Text>
        <Text style={tw`text-sm text-gray-400 mt-4`}>+99 1234567890</Text>
        <View style={tw`flex-row gap-4 mt-4 items-center`}>
            <Button text="Check Out" padding='p-4' color='#40BFFF' textColor='#ffff' onPress={() => {navigation.navigate('ShipTo')}} />
            <IcTrash height={35} width={35}/>
        </View>
    </TouchableOpacity>
  )
}

export default Address