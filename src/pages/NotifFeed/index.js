import React from 'react';
import { ImageBackground, ScrollView, Text, View } from 'react-native';
import tw from 'twrnc';
import { IcList, IcOfferOn, NotificationOn } from '../../assets';
import { BannerSlider, Category, Gap, Header } from '../../components';
import { FlashSale, MegaSale, RecomendedProduct } from '../../components/pages';
import { RowNotification } from '../../components/pages/Notification';

const NotifFeed = ({navigation}) => {
    return (
        <>
            <Header title="Notification" subTitle="Feed" onBack={() => navigation.goBack()} />
            <View style={tw`flex-1 bg-white px-4 w-full py-4`}>
                <RowNotification key={1} 
                    title="The best Title" navigation={() => navigation.navigate("/")}
                    feed="Culpa cillum consectetur labore nulla nulla magna irure. Id veniam culpa officia"
                />
                <Gap height="h-5"/>
                <RowNotification key={2} 
                    title="Jabuary sale 89% all product" navigation={() => navigation.navigate("/")}
                    feed="Culpa cillum consectetur labore nulla nulla magna irure deserunt ex proident commodo"
                />
                <Gap height="h-5"/>
                <RowNotification key={3} 
                    title="Special offer 33% cashback" navigation={() => navigation.navigate("/")}
                    feed="Culpa cillum veniam culpa officia aute dolor amet deserunt ex proident commodo"
                /> 
            </View>
        </>
    )
}

export default NotifFeed